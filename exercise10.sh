#!/bin/bash

git clone git@bitbucket.org:khummel01/swdt-class-todo.git
mv exercise10.sh swdt-class-todo
cd swdt-class-todo
find . -type f -name "*.pyc" -exec git rm {} + 
find . -type f -name "*.class" -exec git rm {} +
find . -type f -name "*.out" -exec git rm {} +
find . -type f -name "*.txt" -exec git rm {} +
git commit -m "Cleaned repository"
git add exercise10.sh
git commit -m "initial add of exercise10"
git push 
